// 
// Here is how to define your module 
// has dependent on mobile-angular-ui
// 
var app = angular.module('MobileAngularUiExamples', [
  'ngRoute',
  'mobile-angular-ui', 'ngTouch',
  
  // touch/drag feature: this is from 'mobile-angular-ui.gestures.js'
  // it is at a very beginning stage, so please be careful if you like to use
  // in production. This is intended to provide a flexible, integrated and and 
  // easy to use alternative to other 3rd party libs like hammer.js, with the
  // final pourpose to integrate gestures into default ui interactions like 
  // opening sidebars, turning switches on/off ..
  'mobile-angular-ui.gestures'
]);

app.run(function($transform) {
  window.$transform = $transform;
  FastClick.attach(document.body);
});

// 
// You can configure ngRoute as always, but to take advantage of SharedState location
// You can configure ngRoute as always, but to take advantage of SharedState location
// feature (i.e. close sidebar on backbutton) you should setup 'reloadOnSearch: false' 
// in order to avoid unwanted routing.
// 
app.config(function($routeProvider) {
  $routeProvider.when('/',              {templateUrl: 'home.html', controller: 'favController', reloadOnSearch: false});
  $routeProvider.when('/gopro',         {templateUrl: 'gopro.html', reloadOnSearch: false}); 
  $routeProvider.when('/toggle',        {templateUrl: 'toggle.html', reloadOnSearch: false}); 
  $routeProvider.when('/tabs',          {templateUrl: 'tabs.html', reloadOnSearch: false}); 
  $routeProvider.when('/accordion',     {templateUrl: 'accordion.html', reloadOnSearch: false}); 
  $routeProvider.when('/overlay',       {templateUrl: 'overlay.html', reloadOnSearch: false}); 
  $routeProvider.when('/forms',         {templateUrl: 'forms.html', reloadOnSearch: false});
  $routeProvider.when('/dropdown',      {templateUrl: 'dropdown.html', reloadOnSearch: false});
  $routeProvider.when('/touch',         {templateUrl: 'touch.html', reloadOnSearch: false});
  $routeProvider.when('/swipe',         {templateUrl: 'swipe.html', reloadOnSearch: false});
  $routeProvider.when('/drag',          {templateUrl: 'drag.html', reloadOnSearch: false});
  $routeProvider.when('/drag2',         {templateUrl: 'drag2.html', reloadOnSearch: false});
  $routeProvider.when('/carousel',      {templateUrl: 'carousel.html', reloadOnSearch: false});
  
  //ADDED BY ZUL
  $routeProvider.when('/detail/:catid',      {templateUrl: 'accordion.html', controller: 'detailController', reloadOnSearch: false});
  //ADDED BY ZUL
});

// 
// `$touch example`
// 

app.directive('toucharea', ['$touch', function($touch){
  // Runs during compile
  return {
    restrict: 'C',
    link: function($scope, elem) {
      $scope.touch = null;
      $touch.bind(elem, {
        start: function(touch) {
          $scope.touch = touch;
          $scope.$apply();
        },

        cancel: function(touch) {
          $scope.touch = touch;  
          $scope.$apply();
        },

        move: function(touch) {
          $scope.touch = touch;
          $scope.$apply();
        },

        end: function(touch) {
          $scope.touch = touch;
          $scope.$apply();
        }
      });
    }
  };
}]);

//
// `$drag` example: drag to dismiss
//
app.directive('dragToDismiss', function($drag, $parse, $timeout){
  return {
    restrict: 'A',
    compile: function(elem, attrs) {
      var dismissFn = $parse(attrs.dragToDismiss);
      return function(scope, elem){
        var dismiss = false;

        $drag.bind(elem, {
          transform: $drag.TRANSLATE_RIGHT,
          move: function(drag) {
            if( drag.distanceX >= drag.rect.width / 4) {
              dismiss = true;
              elem.addClass('dismiss');
            } else {
              dismiss = false;
              elem.removeClass('dismiss');
            }
          },
          cancel: function(){
            elem.removeClass('dismiss');
          },
          end: function(drag) {
            if (dismiss) {
              elem.addClass('dismitted');
              $timeout(function() { 
                scope.$apply(function() {
                  dismissFn(scope);  
                });
              }, 300);
            } else {
              drag.reset();
            }
          }
        });
      };
    }
  };
});

//
// Another `$drag` usage example: this is how you could create 
// a touch enabled "deck of cards" carousel. See `carousel.html` for markup.
//
app.directive('carousel', function(){
  return {
    restrict: 'C',
    scope: {},
    controller: function() {
      this.itemCount = 0;
      this.activeItem = null;

      this.addItem = function(){
        var newId = this.itemCount++;
        this.activeItem = this.itemCount === 1 ? newId : this.activeItem;
        return newId;
      };

      this.next = function(){
        this.activeItem = this.activeItem || 0;
        this.activeItem = this.activeItem === this.itemCount - 1 ? 0 : this.activeItem + 1;
      };

      this.prev = function(){
        this.activeItem = this.activeItem || 0;
        this.activeItem = this.activeItem === 0 ? this.itemCount - 1 : this.activeItem - 1;
      };
    }
  };
});

app.directive('carouselItem', function($drag) {
  return {
    restrict: 'C',
    require: '^carousel',
    scope: {},
    transclude: true,
    template: '<div class="item"><div ng-transclude></div></div>',
    link: function(scope, elem, attrs, carousel) {
      scope.carousel = carousel;
      var id = carousel.addItem();
      
      var zIndex = function(){
        var res = 0;
        if (id === carousel.activeItem){
          res = 2000;
        } else if (carousel.activeItem < id) {
          res = 2000 - (id - carousel.activeItem);
        } else {
          res = 2000 - (carousel.itemCount - 1 - carousel.activeItem + id);
        }
        return res;
      };

      scope.$watch(function(){
        return carousel.activeItem;
      }, function(){
        elem[0].style.zIndex = zIndex();
      });
      
      $drag.bind(elem, {
        //
        // This is an example of custom transform function
        //
        transform: function(element, transform, touch) {
          // 
          // use translate both as basis for the new transform:
          // 
          var t = $drag.TRANSLATE_BOTH(element, transform, touch);
          
          //
          // Add rotation:
          //
          var Dx    = touch.distanceX, 
              t0    = touch.startTransform, 
              sign  = Dx < 0 ? -1 : 1,
              angle = sign * Math.min( ( Math.abs(Dx) / 700 ) * 30 , 30 );
          
          t.rotateZ = angle + (Math.round(t0.rotateZ));
          
          return t;
        },
        move: function(drag){
          if(Math.abs(drag.distanceX) >= drag.rect.width / 4) {
            elem.addClass('dismiss');  
          } else {
            elem.removeClass('dismiss');  
          }
        },
        cancel: function(){
          elem.removeClass('dismiss');
        },
        end: function(drag) {
          elem.removeClass('dismiss');
          if(Math.abs(drag.distanceX) >= drag.rect.width / 4) {
            scope.$apply(function() {
              carousel.next();
            });
          }
          drag.reset();
        }
      });
    }
  };
});

app.directive('dragMe', ['$drag', function($drag){
  return {
    controller: function($scope, $element) {
      $drag.bind($element, 
        {
          //
          // Here you can see how to limit movement 
          // to an element
          //
          transform: $drag.TRANSLATE_INSIDE($element.parent()),
          end: function(drag) {
            // go back to initial position
            drag.reset();
          }
        },
        { // release touch when movement is outside bounduaries
          sensitiveArea: $element.parent()
        }
      );
    }
  };
}]);

//
// For this trivial demo we have just a unique MainController 
// for everything
//
app.controller('MainController', function($rootScope, $scope, $http, $routeParams, $route, $location){
	

  //ADDED BY ZULHATFI AZIZ
  $scope.fullversion = false;
  $scope.trialversion = true;

  var productIds = ['malayphrasebookpro']; // <- Add your product Ids here

if (localStorage.getItem('purchase') == null) {
    
    inAppPurchase
      .restorePurchases()
      .then(function (data) {
        $scope.purchase = data;
        localStorage.setItem('purchase', JSON.stringify($scope.purchase));
        var json = JSON.parse(localStorage.getItem('purchase'));
        var jsonlength = json.length;
        
        if (jsonlength > 0) 
        {
            for (i=0;i<jsonlength;i++)
            {
              if (json[i].productId == productIds) 
              {
                $scope.fullversion = true;
                $scope.trialversion = false;
                  
                //this is how to cancel any purchase that already being made in trial account
//                inAppPurchase
//                    .consume(json[i].type, json[i].receipt, json[i].signature)
//                    .then(function () { 
//                        $scope.fullversion = false;
//                        $scope.trialversion = true;
//                        $rootScope.$apply(function() {
//                            $location.path('/');
//                            AdMob.removeBanner();
//                        });
//                    });
              }
              else{
                $scope.fullversion = false;
                $scope.trialversion = true;
                $rootScope.$apply(function() {
                    $location.path('/');
                    $window.location.reload();
                });
                admob();
                
              }
            } 
        }else{
            $scope.fullversion = false;
            $scope.trialversion = true;
            $rootScope.$apply(function() {
                $location.path('/');
                $window.location.reload();
            });
            admob();
        }
      })
      .catch(function (err) {
        console.log(err);
        
      });
    }else{
        var json = JSON.parse(localStorage.getItem('purchase'));
        
        if(jsonlength > 1){
            var jsonObjArray = []; // = new Array();
            jsonObjArray.push(json);
            var jsonlength = jsonObjArray.length;
        }else{
            var jsonObjArray = json;
            var jsonlength = json.length;
        }
        
        if (jsonlength > 0) 
        {
            for (i=0;i<jsonlength;i++)
            {

              if (jsonObjArray[i].productId == productIds) 
              {
                $scope.fullversion = true;
                $scope.trialversion = false;
                $rootScope.$apply(function() {
                    $location.path('/');
                    AdMob.removeBanner();
                });
                
              }
              else{
                $scope.fullversion = false;
                $scope.trialversion = true;
                $rootScope.$apply(function() {
                    $location.path('/');
                    $window.location.reload();
                });
                admob();
              }
            } 
        }else{
            $scope.fullversion = false;
            $scope.trialversion = true;
            $rootScope.$apply(function() {
                $location.path('/');
                $window.location.reload();
            });
            admob();
        }
    }


  $scope.buy = function () {
    
    inAppPurchase
      .buy('malayphrasebookpro')
      .then(function (data) {
        $scope.purchase = data;
        localStorage.setItem('purchase', JSON.stringify($scope.purchase));
        var json = JSON.parse(localStorage.getItem('purchase'));
        var jsonObjArray = []; // = new Array();
        jsonObjArray.push(json);
        var jsonlength = jsonObjArray.length;
        
        if (jsonlength > 0) 
        {
           
            for (i=0;i<jsonlength;i++)
            {
                //alert(json[i].productId);
              if (jsonObjArray[i].productId == productIds) 
              {
                $scope.fullversion = true;
                $scope.trialversion = false;
                $scope.$apply(function() {
                    $location.path('/');
                    AdMob.removeBanner();
                });
              }
              else{
                $scope.fullversion = false;
                $scope.trialversion = true;
                $rootScope.$apply(function() {
                    $location.path('/');
                    $window.location.reload();
                });
                admob();
              }
            } 
        }else{
            $scope.fullversion = false;
            $scope.trialversion = true;
            $rootScope.$apply(function() {
                $location.path('/');
                $window.location.reload();
            });
            admob();
        }
        //alert('buy1'+ data.transactionId);
        //return inAppPurchase.consume(data.type, data.receipt, data.signature);
      })
      .catch(function (err) {
        console.log(err);
      });

  };

  $scope.restore = function () {
    inAppPurchase
      .restorePurchases()
      .then(function (purchases) {
        console.log(JSON.stringify(purchases));
      })
      .catch(function (err) {
        console.log(err);
      });
  };
    
  $scope.playvoice = function(language) {
        document.addEventListener('deviceready', function () {
            TTS
                .speak({
                    text: language,
                    locale: 'id-ID',
                    rate: 0.75
                }, function () {
                    //alert('success');
                }, function (reason) {
                    //alert(reason);
                });
        }, false);
    };

	//if localstorage empty or null
	if (localStorage.getItem('category') == null) {
	  $http.get('http://www.zulhatfiaziz.com/apps/category.php').
       success(function(response) {
           $scope.category = response;
		       localStorage.setItem('category', JSON.stringify($scope.category));
       });
	}
	//if localstorage is not null
	else{
		$scope.category = JSON.parse(localStorage.getItem('category'));
	}

  if (localStorage.getItem('detail') == null) {
    $http.get('http://www.zulhatfiaziz.com/apps/data_full.php').
     success(function(response) {

       $scope.data = response;
       localStorage.setItem('detail', JSON.stringify($scope.data));
     });
  }
  else{
     $scope.data = JSON.parse(localStorage.getItem('detail'));
  }

  if (localStorage.getItem('fav') == null) {
      var a = [];
      localStorage.setItem('fav', JSON.stringify(a));
  }
	
	$scope.go = function (path) {
	  $location.path(path);
	};
	
	
	$scope.addFav = function(dataID) {
		if (localStorage.getItem('fav') == null) {
			var a = [];
			a.push(dataID);
			localStorage.setItem('fav', JSON.stringify(a));
		}
		else{
			var a = [];
			// Parse the serialized data back into an aray of objects
			a = JSON.parse(localStorage.getItem('fav'));
			// Push the new data (whether it be an object or anything else) onto the array
			if (a.indexOf(dataID) == -1) {
				a.push(dataID);
			}
			// Alert the array value
			//alert(a);  // Should be something like [Object array]
			// Re-serialize the array back into a string and store it in localStorage
			localStorage.setItem('fav', JSON.stringify(a));
		}
		$rootScope.$emit("CallDetailController", {});
    $rootScope.$emit("CallFavController", {});
		//$route.reload();
	};
	
	$scope.delFav = function (dataID) {
		
		var json = JSON.parse(localStorage['fav']);
		
		for (i=0;i<json.length;i++)
		{
			if (json[i] == dataID) 
			{
				json.splice(i,1);	
			}
		}
		localStorage['fav'] = JSON.stringify(json);
		$rootScope.$emit("CallDetailController", {});
		$rootScope.$emit("CallFavController", {});
		//$route.reload();
	}

  $scope.showTrial = function(x){
    return x.cat_name === 'Greetings' || 
        x.cat_name === 'General Conversation' ||
        x.cat_name === 'Numbers';
  };

  $scope.showPro = function(x){
    return x.cat_name !== 'Greetings' && 
        x.cat_name !== 'General Conversation' &&
        x.cat_name !== 'Numbers';
  };
	//ADDED BY ZULHATFI AZIZ
	
	
  $scope.swiped = function(direction) {
    alert('Swiped ' + direction);
  };

  // User agent displayed in home page
  $scope.userAgent = navigator.userAgent;
  
  // Needed for the loading screen
  $rootScope.$on('$routeChangeStart', function(){
    $rootScope.loading = true;
  });

  $rootScope.$on('$routeChangeSuccess', function(){
    $rootScope.loading = false;
  });

  // Fake text i used here and there.
  $scope.lorem = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel explicabo, aliquid eaque soluta nihil eligendi adipisci error, illum corrupti nam fuga omnis quod quaerat mollitia expedita impedit dolores ipsam. Obcaecati.';

  // 
  // 'Scroll' screen
  // 
  var scrollItems = [];

  for (var i=1; i<=100; i++) {
    scrollItems.push('Item ' + i);
  }

  $scope.scrollItems = scrollItems;

  $scope.bottomReached = function() {
    /* global alert: false; */
    alert('Congrats you scrolled to the end of the list!');
  };

  // 
  // Right Sidebar
  // 
  $scope.chatUsers = [
    { name: 'Carlos  Flowers', online: true },
    { name: 'Byron Taylor', online: true },
    { name: 'Jana  Terry', online: true },
    { name: 'Darryl  Stone', online: true },
    { name: 'Fannie  Carlson', online: true },
    { name: 'Holly Nguyen', online: true },
    { name: 'Bill  Chavez', online: true },
    { name: 'Veronica  Maxwell', online: true },
    { name: 'Jessica Webster', online: true },
    { name: 'Jackie  Barton', online: true },
    { name: 'Crystal Drake', online: false },
    { name: 'Milton  Dean', online: false },
    { name: 'Joann Johnston', online: false },
    { name: 'Cora  Vaughn', online: false },
    { name: 'Nina  Briggs', online: false },
    { name: 'Casey Turner', online: false },
    { name: 'Jimmie  Wilson', online: false },
    { name: 'Nathaniel Steele', online: false },
    { name: 'Aubrey  Cole', online: false },
    { name: 'Donnie  Summers', online: false },
    { name: 'Kate  Myers', online: false },
    { name: 'Priscilla Hawkins', online: false },
    { name: 'Joe Barker', online: false },
    { name: 'Lee Norman', online: false },
    { name: 'Ebony Rice', online: false }
  ];

  //
  // 'Forms' screen
  //  
  $scope.rememberMe = true;
  $scope.email = 'me@example.com';
  
  $scope.login = function() {
    alert('You submitted the login form');
  };

  // 
  // 'Drag' screen
  // 
  $scope.notices = [];
  
  for (var j = 0; j < 10; j++) {
    $scope.notices.push({icon: 'envelope', message: 'Notice ' + (j + 1) });
  }

  $scope.deleteNotice = function(notice) {
    var index = $scope.notices.indexOf(notice);
    if (index > -1) {
      $scope.notices.splice(index, 1);
    }
  };
});

//ADDED BY ZULHATFI AZIZ
app.controller('detailController', function($rootScope, $scope, $http, $routeParams) {
  $rootScope.$on("CallDetailController", function(){
    var param = $routeParams.catid;

    //if localstorage empty or null
    if (localStorage.getItem('detail') == null) {
      $http.get('http://www.zulhatfiaziz.com/apps/data_full.php').
       success(function(response) {

         $scope.data = response;
         $scope.catid = $routeParams.catid;
        localStorage.setItem('detail', JSON.stringify($scope.data));
         $scope.favdata = JSON.parse(localStorage.getItem('fav'));
       });
    }
    //if localstorage is not null
    else{
      $scope.catid = $routeParams.catid;
      //localStorage.removeItem('detail' + $routeParams.catid);
      $scope.data = JSON.parse(localStorage.getItem('detail'));
      $scope.favdata = JSON.parse(localStorage.getItem('fav'));
    }
  });
  
  $rootScope.$emit("CallDetailController", {});
    
});

app.filter('customOrder',function($filter,$routeParams)
{
    return function(x, prediate,cat){
      var catid = $routeParams.catid;
        
      if(catid == '3'){
          console.log('YES');
       return x;
      }
      else
      {
        var result = $filter("orderBy")(x, prediate);
        return result;
      }

    }
});

app.controller('favController', function($rootScope, $scope, $http) {
  $rootScope.$on("CallFavController", function(){
    if (localStorage.getItem('detail') == null) {
       $http.get('http://www.zulhatfiaziz.com/apps/data_full.php').
       success(function(response) {
          $scope.fav = response;
          $scope.filterBy = JSON.parse(localStorage.getItem('fav'));

       });
     }
     else{
          $scope.fav = JSON.parse(localStorage.getItem('detail'));
          $scope.filterBy = JSON.parse(localStorage.getItem('fav'));
     }
  });
  $rootScope.$emit("CallFavController", {});
});

AppRate.preferences = {
  openStoreInApp: true,
  displayAppName: 'English - Malay Phrasebook',
  usesUntilPrompt: 2,
  promptAgainForEachNewVersion: false,
  storeAppURL: {
    //ios: '<my_app_id>',
    android: 'market://details?id=io.cordova.MalayPhrasebook',
    //windows: 'ms-windows-store://pdp/?ProductId=<the apps Store ID>',
    //blackberry: 'appworld://content/[App Id]/',
    //windows8: 'ms-windows-store:Review?name=<the Package Family Name of the application>'
  },
  customLocale: {
    title: "Rate this app",
    message: "If you enjoy using this app, would you mind taking a moment to rate it? It won’t take more than a minute. Thanks for your support!",
    cancelButtonLabel: "No, Thanks",
    laterButtonLabel: "Remind Me Later",
    rateButtonLabel: "Rate It Now"
  }
};

AppRate.promptForRating();

var admob = function() {
    var admobid = {};

    // TODO: replace the following ad units with your own
    if( /(android)/i.test(navigator.userAgent) ) { 
      admobid = { // for Android
        banner: 'ca-app-pub-6440367096924168/5776976737',
        interstitial: 'ca-app-pub-6440367096924168/5593029931'
      };
    }


    function initApp() {
      if (! AdMob ) { console.log( 'admob plugin not ready' ); return; }

      // this will create a banner on startup
      AdMob.createBanner( {
        adId: admobid.banner,
        position: AdMob.AD_POSITION.BOTTOM_CENTER,
        //isTesting: true, // TODO: remove this line when release
        adSize: 'SMART_BANNER',
        overlap: false,
        offsetTopBar: false,
        bgColor: 'black'
      } );

      // this will load a full screen ad on startup
//      AdMob.prepareInterstitial({
//        adId: admobid.interstitial,
//        //isTesting: true, // TODO: remove this line when release
//        autoShow: true
//      });

    }


    if(( /(ipad|iphone|ipod|android|windows phone)/i.test(navigator.userAgent) )) {
        document.addEventListener('deviceready', initApp, false);
        
    } else {
        initApp();
    }
};

app.filter('getById', function() {
  return function(input, id) {
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (+input[i].id == +id) {
        return input[i];
      }
    }
    return null;
  }
});

app.filter('inArray', function($filter){
    return function(list, arrayFilter, element){
        if(arrayFilter){
            return $filter("filter")(list, function(listItem){
                return arrayFilter.indexOf(listItem[element]) != -1;
            });
        }
    };
});


//ADDED BY ZULHATFI AZIZ