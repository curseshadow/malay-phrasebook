
angular.module('starter', ['ngRoute'])

.run(function($transform) {
  window.$transform = $transform;
})

.controller('AppCtrl', function($scope) {

  var productIds = ['malayphrasebookpro']; // <- Add your product Ids here

  $scope.loadProducts = function () {
    inAppPurchase
      .getProducts(productIds)
      .then(function (products) {
        $scope.products = products;
      })
      .catch(function (err) {
        console.log(err);
      });
  };

  $scope.buy = function (productId, $location) {
    inAppPurchase
      .buy(productId)
      .then(function (data) {
        console.log(JSON.stringify(data));
        console.log('consuming transactionId: ' + data.transactionId);
        return inAppPurchase.consume(data.type, data.receipt, data.signature);
      })
      .then(function () {
        console.log('consume done!');
        $location.path("/PRO/index.html");
      })
      .catch(function (err) {
        console.log(err);
      });

  };

  $scope.restore = function () {
    inAppPurchase
      .restorePurchases()
      .then(function (purchases) {
        console.log(JSON.stringify(purchases));
      })
      .catch(function (err) {
        console.log(err);
      });
  };

});