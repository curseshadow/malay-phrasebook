var admobid = {};

// TODO: replace the following ad units with your own
if( /(android)/i.test(navigator.userAgent) ) { 
  admobid = { // for Android
    banner: 'ca-app-pub-6440367096924168/5776976737',
    interstitial: 'ca-app-pub-6440367096924168/5593029931'
  };
}


function initApp() {
  if (! AdMob ) { console.log( 'admob plugin not ready' ); return; }

  // this will create a banner on startup
  AdMob.createBanner( {
    adId: admobid.banner,
    position: AdMob.AD_POSITION.BOTTOM_CENTER,
    //isTesting: true, // TODO: remove this line when release
    adSize: 'SMART_BANNER',
    overlap: false,
    offsetTopBar: false,
    bgColor: 'black'
  } );

  // this will load a full screen ad on startup
  AdMob.prepareInterstitial({
    adId: admobid.interstitial,
    //isTesting: true, // TODO: remove this line when release
    autoShow: true
  });

}


if(( /(ipad|iphone|ipod|android|windows phone)/i.test(navigator.userAgent) )) {
    document.addEventListener('deviceready', initApp, false);
    
} else {
    initApp();
}
