# cordova-plugin-clipboard

Provides copying and pasting text to/from clipboard.

## Platforms

- iOS
- Android
- WP8

## Installation

	cordova plugin add https://github.com/xxsnakerxx/cordova-plugin-clipboard

## Notes

- Supports only text content!!!
- For Android the minimum supported __API Level is 11__.
- The Windows Phone platform __doesn't allow applications to read the content of the clipboard__. Using the paste method will return an error.

## Usage

	cordova.plugins.clipboard.copy(text);

	cordova.plugins.clipboard.paste(onSuccess, onError);

	function onSuccess(text) {
		console.log(text);
	}
