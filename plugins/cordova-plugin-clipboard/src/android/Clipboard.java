/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Coloreight LLC
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.coloreight.plugin.clipboard;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.ClipboardManager;
import android.content.ClipData;
import android.content.ClipDescription;

public class Clipboard extends CordovaPlugin {

    private static final String ACTION_COPY = "copy";
    private static final String ACTION_PASTE = "paste";

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals(ACTION_COPY)) {
            this.copy(args.getString(0), callbackContext);
        }
        else if (action.equals(ACTION_PASTE)) {
            this.paste(callbackContext);
        }
        else {
            return false;
        }

        return true;
    }


    public void copy(final String text, final CallbackContext callbackContext) {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                ClipboardManager clipboard = (ClipboardManager)cordova.getActivity().getSystemService(Context.CLIPBOARD_SERVICE);

                try {
                    ClipData clip = ClipData.newPlainText("Public.Text", text);

                    clipboard.setPrimaryClip(clip);

                    callbackContext.success(text);
                }
                catch (Exception e) {
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, e.toString()));
                }
            }
        });
    }

    public void paste(final CallbackContext callbackContext) {
        cordova.getActivity().runOnUiThread(new Runnable() {
            public void run() {
                ClipboardManager clipboard = (ClipboardManager)cordova.getActivity().getSystemService(Context.CLIPBOARD_SERVICE);

                if (!clipboard.getPrimaryClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.NO_RESULT));
                }

                try {
                    ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);
                    String text = item.getText().toString();

                    callbackContext.success(text);
                }
                catch (Exception e) {
                    callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, e.toString()));
                }
            }
        });
    }
}
