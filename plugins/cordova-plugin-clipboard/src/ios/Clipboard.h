#import <Cordova/CDVPlugin.h>

@interface Clipboard : CDVPlugin

- (void)copy:(CDVInvokedUrlCommand *)command;
- (void)paste:(CDVInvokedUrlCommand *)command;

@end
